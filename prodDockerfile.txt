FROM php:7.4-apache

RUN docker-php-ext-install mysqli pdo pdo_mysql


COPY ./apache/conf /etc/apache2/sites-available/
COPY ./apache/certs /etc/ssl


RUN cat ./apache/default/apache2.conf >> /etc/apache2/apache2.conf

RUN mkdir -p /var/www/html/uploads
COPY ./apache/upconf /var/www/html/uploads

RUN chown www-data:www-data /var/www/html/uploads
RUN chmod 755 /var/www/html/uploads



RUN a2enmod rewrite ssl headers
RUN a2ensite default-ssl

RUN service apache2 restart

