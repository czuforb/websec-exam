<?php

session_start();
$email_message = '';
$password_message = '';

require_once __DIR__ . '/../helpers/regex.php';
require_once __DIR__ . '/../helpers/timeout.php';
require_once __DIR__ . '/../db/db.php';

if ($_POST) {
    $email = $_POST['email'] ?? NULL;
    $password = $_POST['password'] ?? NULL;

    if (!validEmail($email)) {
        $email_message = "Invalid email";
    }
    if (!strongPassword($password)) {
        $password_message = "Password must be atleast 8 characters, contain numbers, lowercase and uppercase characters.";
    }

    if (validEmail($email) && strongPassword($password)) {
        $statement = $db->prepare('SELECT * FROM users WHERE email = :email');
        $statement->bindValue('email', $email);
        $statement->execute();
        $user = $statement->fetch();
        if (!$user) {
            $statement = $db->prepare('INSERT INTO users (email, password) VALUES (:email,:password)');
            $statement->bindValue('email', $email);
            $statement->bindValue('password', password_hash($password, PASSWORD_DEFAULT));
            $statement->execute();
            header('Location: /');
            exit();
        }

        $timed_out = timeout($user, $db);
        if ($timed_out) {
            $password_message = 'Slow down';
        } else {
            $email_message = 'Email already exists';
        }
    }
}



require_once __DIR__ . '/../templates/sign-up.php';