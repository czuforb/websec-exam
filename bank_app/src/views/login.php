<?php
$general_error_message = '';
session_start();
require_once __DIR__ . '/../db/db.php';
require_once __DIR__ . '/../helpers/regex.php';
require_once __DIR__ . '/../helpers/timeout.php';
require_once __DIR__ . '/../helpers/csrf.php';


if ($_POST) {
    if (!is_csrf_valid()) {
        echo "invalid csrf";
        exit;
    }
    postRunner($general_error_message, $db);
}

require_once __DIR__ . '/../templates/login.php';


// Runs the entire validation process
function postRunner(&$general_error_message, $db)
{
    try {
        $email = $_POST['email'] ?? NULL;
        $password = $_POST['password'] ?? NULL;
        $error_message = 'Invalid e-mail or password';

        if (!validEmail($email) || !strongPassword($password)) throw new Exception($error_message);

        $statement = $db->prepare('SELECT * FROM users WHERE email = :email');
        $statement->bindValue('email', $email);
        $statement->execute();
        $user = $statement->fetch();

        if (!$user) throw new Exception($error_message);

        // this COULD have timed out error message, but then we tell the bruteforcer there is a timeout
        // which would allow him to create more dedicated attacks
        $timed_out = timeout($user, $db);
        if ($timed_out) throw new Exception('Slow down');
        $same_passwords = password_verify($password, $user->password);
        if (!$same_passwords) throw new Exception($error_message);

        $_SESSION['email'] = $user->email;
        $_SESSION['user_id'] = $user->user_id;
        $_SESSION['is_employee'] = $user->is_employee;
        switch ($user->is_employee) {
            case 0:
                header('location: /dashboard');
                break;
            case 1:
                header('location: /employee-dashboard');
                break;
            default:
                throw new Exception($error_message);
        }
    } catch (Exception $e) {
        $general_error_message = $e->getMessage();
    }
}