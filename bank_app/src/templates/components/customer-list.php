<?php
$error_message = '';
require_once __DIR__ . '/../../db/db.php';

try {
    $statement = $db->prepare('SELECT chat_id, customer_fk, email FROM chats JOIN users ON customer_fk = user_id');
    $statement->execute();
    $customer_chat_list = $statement->fetchAll();
    if (!$customer_chat_list) {
        $error_message = 'No users to display.';
    }
} catch (Exception $ex) {
    echo $ex;
    exit();
}
?>

<ul id="customer-list">
    <?php
    if ($error_message) {
    ?>
    <p><?= htmlspecialchars($error_message) ?></p>
    <?php
    }
    foreach ($customer_chat_list as $customer_chat) {
    ?>
    <li class="customer-list-item" data-id="<?= htmlspecialchars($customer_chat->chat_id) ?>">
        <?= htmlspecialchars($customer_chat->email) ?></li>
    <?php
    }
    ?>
</ul>