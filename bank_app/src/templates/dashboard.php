<?php
$title = "Dashboard";
require_once __DIR__ . '/components/head.php';
?>
<header>
    <a href="/logout" style="margin-right: 30px;">Logout</a>
    <a href="/dashboard" style="margin-right: 30px;">Dashboard</a>
    <a href="/upload">Upload</a>
</header>
<h1>Dashboard</h1>
<section>
    <p>Welcome <?= $email ?> to our beautiful dashboard... Start chatting with support right away!</p>
    <button id="display-chat">Talk to support</button>
    <?php require_once __DIR__ . '/components/chat.php' ?>
</section>

<?php require_once __DIR__ . '/components/footer.php' ?>