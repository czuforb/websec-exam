<?php
$title = "Sign up";
require_once __DIR__ . '/components/head.php';
require_once __DIR__ . '/../helpers/csrf.php';
?>

<h1>Sign up!</h1>
<form action="" method="POST">
    <?php set_csrf() ?>
    <input type="email" name="email" placeholder="Email" value="">
    <p class="error"><?= $email_message ?></p>
    <input type="password" name="password" placeholder="Password" value="">
    <p class="error"><?= $password_message ?></p>
    <button>Submit</button>
</form>
<p>Password must have:</p>
<ul>
    <li>8 characters</li>
    <li>uppercase letters</li>
    <li>lowercase letters</li>
    <li>numbers</li>

</ul>
<p>
    <a href="/">Login</a>
</p>
<?php
require_once __DIR__ . '/components/footer.php';
?>