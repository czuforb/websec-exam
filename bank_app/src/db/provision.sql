CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_employee` tinyint(1) NOT NULL DEFAULT 0
) ENGINE='InnoDB' COLLATE 'utf8mb4_bin';

CREATE TABLE IF NOT EXISTS `chats` (
  `chat_id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `employee_fk` bigint unsigned NOT NULL,
  `customer_fk` bigint unsigned NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8mb4_bin';

CREATE TABLE IF NOT EXISTS `chat_messages` (
  `chat_fk` bigint unsigned NOT NULL,
  `message_id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `sender_fk` bigint unsigned NOT NULL,
  `message_body` varchar(255) NOT NULL,
  `message_created` bigint NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8mb4_bin';

CREATE TABLE IF NOT EXISTS `logins`(
  `login_id`bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `login_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `user_fk` bigint unsigned NOT NULL 
) ENGINE='InnoDB' COLLATE 'utf8mb4_bin';

CREATE ROLE IF NOT EXIST public;

GRANT SELECT,INSERT ON bank.* TO public;

GRANT public TO 'pubuser'@'%';