<?php


// true if timedout else false
function timeout($user, $db)
{
    try {
        // insert
        $statement = $db->prepare('INSERT INTO logins (user_fk) VALUES (:user_id)');
        $statement->bindValue('user_id', $user->user_id);
        $statement->execute();

        $success = $db->lastInsertId();
        if (!$success) return false;

        // compare last login
        $statement = $db->prepare('SELECT * FROM logins WHERE user_fk = :user_id ORDER BY login_time DESC LIMIT 4');
        $statement->bindValue('user_id', $user->user_id);
        $statement->execute();
        $attempts = $statement->fetchAll();


        $allowed_attempts = 4;
        $current_time = time();
        $timeoutInSeconds = 5 * 60;
        $count = 0;
        for ($i = 0; $i < count($attempts); $i++) {
            $attempt = $attempts[$i];
            $attempt_login_time = $attempt->login_time;
            $attempt_login_time_str = strtotime($attempt_login_time);
            if ($current_time - $attempt_login_time_str < $timeoutInSeconds) $count++;
        }
        if ($count === $allowed_attempts) return true;
    } catch (Exception $ex) {
        echo 'Contact admin';
    }
}