<?php
session_start();

if(! isset($_SESSION['user_id']) || ! ctype_digit($_SESSION['user_id'])){
    http_response_code(403);
    header('location: ../');
    exit();
}

if($_SERVER['REQUEST_METHOD'] === 'GET'){
    echo $_SESSION['user_id'];
}