<?php
session_start();

if(! isset($_SESSION['user_id']) || ! ctype_digit($_SESSION['user_id'])){
    http_response_code(403);
    header('location: ../');
    exit();
}
require_once __DIR__ . '/../db/db.php';
require_once __DIR__ . '/../helpers/csrf.php';

if( ! is_csrf_valid()){
    http_response_code(403);
    echo 'Tryna hack, huh?';
    exit();
}

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $user_id = $_SESSION['user_id'];
    // $message = htmlspecialchars($_POST['message_body']);
    // $chat_id = htmlspecialchars($_POST['chat_id']);
    $message = $_POST['message_body'];
    $chat_id = $_POST['chat_id'];

    if(!isset($message)){
        http_response_code(500);
        echo 'Please type minimum one character.';
        exit();
    }

    if(strlen($message) < 1){
        http_response_code(500);
        echo 'Please type minimum one character.';
        exit();
    }
    
    if(strlen($message) > 255){
        http_response_code(500); // the code is not that good
        echo 'Maximum characters reached.';
        exit();
    }


    if($message && $chat_id){
        try{

            $statement = $db->prepare('SELECT customer_fk FROM chats WHERE chat_id = :chat_id LIMIT 1');
            $statement->bindValue('chat_id', $chat_id);
            $statement->execute();
            $customer_fk = $statement->fetch();

            if($_SESSION['is_employee'] == 0 && $customer_fk->customer_fk !== $user_id){
                http_response_code(403);
                echo 'Not your chat';
                exit();
            }

            $statement = $db->prepare('INSERT INTO chat_messages VALUES(:chat_fk, null, :sender_fk, :message_body, UNIX_TIMESTAMP())');
            $statement->bindValue('chat_fk', $chat_id);
            $statement->bindValue('message_body', $message);
            $statement->bindValue('sender_fk', $user_id);
            $statement->execute();
            exit();
        }catch(Exception $ex){
            http_response_code(500);
            // echo $ex; <- sends back the error, no bueno
            exit();
        }
    }
}
