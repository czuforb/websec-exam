<?php
session_start();

if(! isset($_SESSION['user_id']) || ! ctype_digit($_SESSION['user_id'])){
    http_response_code(403);
    header('location: ../');
    exit();
}

$user_id = $_SESSION['user_id'];

require_once __DIR__ . '/../db/db.php';

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    try{

        $statement = $db->prepare('SELECT chat_id FROM chats WHERE customer_fk = :customer_id LIMIT 1');
        $statement->bindValue('customer_id', $user_id);
        $statement->execute();
        $chat = $statement->fetch();

        if($chat){
            exit();
        }

        $statement = $db->prepare('SELECT user_id FROM users WHERE is_employee = 1 ORDER BY RAND() LIMIT 1');
        $statement->execute();
        $employee_id = $statement->fetch();
    
        if(!$employee_id){
            http_response_code(500);
            echo 'Could not find any employee';
            exit();
        }
    
        $statement = $db->prepare('INSERT INTO chats VALUES(null, :employee_id, :customer_id)');
        $statement->bindValue('employee_id', $employee_id->user_id);
        $statement->bindValue('customer_id', $user_id);
        $statement->execute();
        $lastId = $db->lastInsertId();
    
        header('content-type: application/json');
        echo '{"chat_id":"'. htmlspecialchars($lastId) .'"}';
    
    }catch(Exception $ex){
        echo $ex;
        exit();
    }
}




