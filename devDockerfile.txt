FROM php:7.4-apache

RUN docker-php-ext-install mysqli pdo pdo_mysql


RUN mkdir -p /var/www/html/uploads
RUN chown www-data:www-data /var/www/html/uploads
RUN chmod 755 /var/www/html/uploads

RUN a2enmod rewrite